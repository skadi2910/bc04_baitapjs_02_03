// BÀI 1
function getSalaries() {
  var wageperDay = parseFloat(document.getElementById("txt-wages").value);
  var days = document.getElementById("txt-days").value * 1;
  var salaries = wageperDay * days;
  document.getElementById("result_1").innerText =
    "Tiền lương của bạn là: " + new Intl.NumberFormat("vn-VN").format(salaries);
}

// BÀI 2
function getAverage() {
  var num1 = parseFloat(document.getElementById("txt-num1").value);
  var num2 = parseFloat(document.getElementById("txt-num2").value);
  var num3 = parseFloat(document.getElementById("txt-num3").value);
  var num4 = parseFloat(document.getElementById("txt-num4").value);
  var num5 = parseFloat(document.getElementById("txt-num5").value);
  var sum = num1 + num2 + num3 + num4 + num5;
  var average = sum / 5;
  document.getElementById("result_2").innerHTML = +average;
  /*  ARRAY
  var num_arr = [];
  num_arr.push(num1, num2, num3, num4, num5);
  console.log("num_arr: ", num_arr);
  var num_arr_sum = num_arr.reduce(function (a, b) {
    return a + b;
  }, 0);
  console.log("num_arr_sum: ", num_arr_sum);
  var num_arr_avg = num_arr_sum / num_arr.length;
  console.log("num_arr_avg: ", num_arr_avg);
*/
}

// BÀI 3
function getVNDfromUSD() {
  var exchangeRate = 23500;
  var usDollar = Math.abs(document.getElementById("txt-usd").value);
  var vnDong = usDollar * exchangeRate;
  document.getElementById("result_3").innerText = new Intl.NumberFormat(
    "vn-VN",
    {
      style: "currency",
      currency: "VND",
    }
  ).format(vnDong);
}

// BÀI 4
function getArea_Perimeter() {
  var r_width = Math.abs(document.getElementById("txt-width").value);
  var r_length = Math.abs(document.getElementById("txt-height").value);
  var area = r_width * r_length;
  var perimeter = (r_width + r_length) * 2;
  document.getElementById("result_4").innerHTML = `Diện Tích : ${area}
    Chu Vi: ${perimeter}`;
}

// BÀI 5
function getSum_ofNumber() {
  var num = parseInt(document.getElementById("txt-num").value);
  var unit = num % 10;
  var tenth = parseInt((num / 10) % 10);
  var hundreth = parseInt(num / 100);
  var sum = unit + tenth + hundreth;
  if (hundreth == 0) {
    document.getElementById("result_5").innerHTML =
      tenth + " + " + unit + " = " + sum;
  } else {
    document.getElementById("result_5").innerHTML =
      hundreth + " + " + tenth + " + " + unit + " = " + sum;
  }
}
